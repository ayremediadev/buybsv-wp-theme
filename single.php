<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<main role="main" class="clearfix">
	<?php while ( have_posts() ) : the_post(); ?>

		<?php if( get_post_type( get_the_ID() ) == 'videos' ):  ?>
			<?php get_template_part( 'loop-templates/content', 'video' ); ?>
		<?php else :  ?>
			<?php get_template_part( 'loop-templates/content', 'single' ); ?>
		<?php endif; ?>	

	<?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>

