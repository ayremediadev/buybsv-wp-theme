<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta property='og:title' content='BuyBSV'/>
	<meta property='og:image' content='http://buybsv.com/wp-content/uploads/2019/07/06_BBSV_Twitter_1024x512.gif'/>
	<meta property='og:description' content='<?php echo get_bloginfo('description'); ?>'/>
	<meta property='og:url' content='<?php global $wp; echo home_url( $wp->request ) ?>' />

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<script>
		var ssiSocialTrackingCode = 'baf27545ad0bd257';
	</script>
	<script>
		!function(s,e){s._ssiAsyncQueue=[],s._ssiAddBeacon=function(e){s._ssiAsyncQueue.push({methodName:"_ssiAddBeacon",args:[e,(new Date).getTime()]})},s._ssiLinkCustomer=function(e){s._ssiAsyncQueue.push({methodName:"_ssiLinkCustomer",args:[e,(new Date).getTime()]})},s._ssiLoginCustomer=function(e){s._ssiAsyncQueue.push({methodName:"_ssiLoginCustomer",args:[e,(new Date).getTime()]})},s._ssiLogPurchase=function(e){s._ssiAsyncQueue.push({methodName:"_ssiLogPurchase",args:[e,(new Date).getTime()]})};var n=e.createElement("script");n.async=1,n.src="https://app.socialsignin.net/assets/v2/js/social-tracking.js";var t=e.getElementsByTagName("script")[0];t.parentNode.insertBefore(n,t)}(window,document);
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142410135-1"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());

	 gtag('config', 'UA-142410135-1');
	</script>
</head>
<!-- style="background-image: url('https://buybsv.ga/wp-content/uploads/2019/05/main-image.png'); background-repeat: no-repeat;background-size: cover;background-position: top center;" -->
<body <?php body_class(); ?>>
	<div class="jumbotron jumbotron-fluid px-0 py-0 d-flex align-items-center" style="height: auto;" >
		<img src="https://buybsv.com/wp-content/uploads/2019/05/main-image.png">
		<div class="container-fluid" style="position: absolute;z-index: 99;">
			<div class="row mb-2">
            	<a href="<?= esc_url(home_url('/')); ?>" class="navbar-brand mx-auto"> 
					<?php 
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
					echo '<img src="' . esc_url( $custom_logo_url ) . '" alt="Craig Wright"></a>';?>
				</a>
			</div>
			<div class="row d-block text-center">
				<?php 
					$header_text = '<h3 class="text-white text-uppercase mb-0">' . get_field('header_text_1') . '<br/>' . get_field('header_text_2') . '</h3>';
					echo $header_text;
				?>
			</div>
		</div>
	</div>