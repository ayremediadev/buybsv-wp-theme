<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_filter( 'body_class', 'understrap_body_classes' );

if ( ! function_exists( 'understrap_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function understrap_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}
}

// Removes tag class from the body_class array to avoid Bootstrap markup styling issues.
add_filter( 'body_class', 'understrap_adjust_body_class' );

if ( ! function_exists( 'understrap_adjust_body_class' ) ) {
	/**
	 * Setup body classes.
	 *
	 * @param string $classes CSS classes.
	 *
	 * @return mixed
	 */
	function understrap_adjust_body_class( $classes ) {

		foreach ( $classes as $key => $value ) {
			if ( 'tag' == $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;

	}
}

// Filter custom logo with correct classes.
add_filter( 'get_custom_logo', 'understrap_change_logo_class' );

if ( ! function_exists( 'understrap_change_logo_class' ) ) {
	/**
	 * Replaces logo CSS class.
	 *
	 * @param string $html Markup.
	 *
	 * @return mixed
	 */
	function understrap_change_logo_class( $html ) {

		$html = str_replace( 'class="custom-logo"', 'class="img-fluid"', $html );
		$html = str_replace( 'class="custom-logo-link"', 'class="navbar-brand custom-logo-link"', $html );
		$html = str_replace( 'alt=""', 'title="Home" alt="logo"', $html );

		return $html;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */

if ( ! function_exists ( 'understrap_post_nav' ) ) {
	function understrap_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}
		?>
		<nav class="container navigation post-navigation">
			<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'understrap' ); ?></h2>
			<div class="row nav-links justify-content-between">
				<?php
				if ( get_previous_post_link() ) {
					previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'understrap' ) );
				}
				if ( get_next_post_link() ) {
					next_post_link( '<span class="nav-next">%link</span>', _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'understrap' ) );
				}
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
}

if ( ! function_exists( 'understrap_pingback' ) ) {
	/**
	 * Add a pingback url auto-discovery header for single posts of any post type.
	 */
	function understrap_pingback() {
		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="' . esc_url( get_bloginfo( 'pingback_url' ) ) . '">' . "\n";
		}
	}
}
add_action( 'wp_head', 'understrap_pingback' );

if ( ! function_exists( 'understrap_mobile_web_app_meta' ) ) {
	/**
	 * Add mobile-web-app meta.
	 */
	function understrap_mobile_web_app_meta() {
		echo '<meta name="mobile-web-app-capable" content="yes">' . "\n";
		echo '<meta name="apple-mobile-web-app-capable" content="yes">' . "\n";
		echo '<meta name="apple-mobile-web-app-title" content="' . esc_attr( get_bloginfo( 'name' ) ) . ' - ' . esc_attr( get_bloginfo( 'description' ) ) . '">' . "\n";
	}
}
add_action( 'wp_head', 'understrap_mobile_web_app_meta' );

// Creates Game Reviews Custom Post Type
function directors_init() {
	$args = array(
		'label' => 'Directors',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'directors'),
			'query_var' => true,
			'menu_icon' => 'dashicons-businessman',
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'directors', $args );
}
add_action( 'init', 'directors_init' );

// Creates Videos Custom Post Type
function videos_init() {
	$args = array(
		'label' => 'Videos',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'videos'),
			'query_var' => true,
			'menu_icon' => 'dashicons-video-alt3',
			'taxonomies' => array('post_tag'),
			'all_items' => __( 'All Videos' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Video' ), 
			'update_item' => __( 'Update Video' ),
			'add_new_item' => __( 'Add New Video' ),
			'new_item_name' => __( 'New Video Name' ),
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'videos', $args );
}
add_action( 'init', 'videos_init' );


// Creates Ecosystem Custom Post Type
function ecosystem_init() {
	$args = array(
		'label' => 'Ecosystem',
			'public' => false,
			'show_ui' => true,
			'capability_type' => 'page',
			'hierarchical' => true,
			'rewrite' => array('slug' => 'ecosystems'),
			'taxonomies' => array( 'ecosystems_list' ),
			'query_var' => true,
			'menu_icon' => 'dashicons-groups',
			'supports' => array(
					'title',
					'editor',
					'excerpt',
					'custom-fields',
					'revisions',
					'thumbnail',
					'author',
					'page-attributes',)
			);
	register_post_type( 'ecosystem', $args );
}
add_action( 'init', 'ecosystem_init' );

function customposttype_taxonomy() {  
	register_taxonomy(  
		 'ecosystems_list',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
		 'ecosystem',        //post type name
		 array(  
			  'hierarchical' => true,  
			  'label' => 'Ecosystems List',  //Display name
			  'query_var' => true,
			  'rewrite' => array(
					'slug' => 'list', // This controls the base slug that will display before each term
					'with_front' => false // Don't display the category base before 
			  )
		 )  
	);  
}  
add_action( 'init', 'customposttype_taxonomy');


// Creates Services Custom Post Type
function services_init() {
	$args = array(
		'label' => 'Services',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'rewrite' => array('slug' => 'services'),
			'query_var' => true,
			'menu_icon' => 'dashicons-screenoptions',
			'all_items' => __( 'All Services' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Service' ), 
			'update_item' => __( 'Update Service' ),
			'add_new_item' => __( 'Add New Service' ),
			'new_item_name' => __( 'New Service Name' ),
			'supports' => array(
					'title',
					'editor',
					'revisions',
					'thumbnail',

					'page-attributes',)
			);
	register_post_type( 'services', $args );
}
add_action( 'init', 'services_init' );

function my_taxonomy_query( $args, $field, $post_id ) {
    
	// modify args
	$args['orderby'] = 'count';
	$args['order'] = 'ASC';
	
	
	// return
	return $args;
	
}

add_filter('acf/fields/taxonomy/query', 'my_taxonomy_query', 10, 3);