<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

  <!-- Footer -->
  <footer class="py-4 brand_bgwhite clearfix">
    <div class="<?php echo esc_attr( $container ); ?>">
      <div class="row align-items-center justify-content-between">
        <div class="col-12 col-md-6 order-2 order-md-1">
          <?php wp_nav_menu(
            array(
              'theme_location'  => 'footer',
              'container_class' => '',
              'container_id'    => 'FooterNavigation',
              'menu_class'      => 'nav footernav justify-content-center justify-content-md-start',
              'fallback_cb'     => '',
              'menu_id'         => 'main-menu',
              'depth'           => 2,
              'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
            )
          ); ?>
        </div>
        <!-- <div class="col-12 col-md- order-1 order-md-2 text-center">
          <a href="<?php //echo get_home_url(); ?>" class="footer-brand mx-auto">
            <?php 
            // $image = get_field('footer_logo', 'option');
            // if( !empty($image) ): ?>
              <img src="<?php //echo $image['url']; ?>" alt="<?php //echo $image['alt']; ?>" class="img-fluid" />
            <?php //endif; ?>
          </a>
        </div> -->
        <div class="col-12 col-md-6 order-3 order-md-3 text-center text-md-right">
         <!--  <ul class="nav footernav footernav_icons justify-content-center justify-content-md-end">
            <li class="nav-item">
              <a class="nav-link" href="<?php //the_field('twitter_url', 'option'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php //the_field('youtube_url', 'option'); ?>"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
            </li>
          </ul> -->
          <!-- <p class="mb-0" style="color: #919191">Powered by Coinify</p> -->
          <p class="mb-0 px-0 text-center" style="color: #919191">&copy Copyright 2019 Coinify. All rights reserved.</p>
        </div>
		<div class="col-12 col-md-12 order-12 order-md-12 text-center text-md-center" id="disclaimer">
			<p class="mb-0 px-0 text-center" style="padding: .5rem 1rem !important; color: #919191">
				This is a joint effort between <a target="_blank" href="https://coingeek.com/" title="CoinGeek">CoinGeek</a> and <a target="_blank" href="https://coinify.com/" title="Coinify">Coinify</a> with CoinGeek profits donated to the <a target="_blank" href="https://bitcoinassociation.net/" title="Bitcoin Association">Bitcoin Association</a> for the benefit of the original Bitcoin SV ecosystem.
			</p>
		</div> 
      </div>
      <!-- <div class="row mt-3 align-items-center justify-content-between disclaimer">
        <div class="col-12">
          <p class="mb-0 px-0 text-center">&copy Copyright 2019 Ayre Media Group. All right reserved.</p>
        </div>
      </div> -->
    </div>
    <!-- /.container -->
  </footer>
  <?php if( get_field('sponsorship_content', 'option') ): ?>
  <section class="sponsor_footer py-3 text-white bg-dark">
    <div class="container">
      <div class="row align-items-center justify-content-between">
        <div class="col text-center">
          <?php the_field('sponsorship_content', 'option', false, false); ?>
        </div>
      </div>
    </div>
  </section>
  <?php endif; ?>

<?php wp_footer(); ?>
<script type="text/javascript">
<?php the_field('footer_custom_js', 'option'); ?>
</script>
<?php if( !is_admin() ):  ?>
  <script type="text/javascript">
  var adminbar = jQuery( "#wpadminbar" ).height();
  jQuery('.navbar').css('top', adminbar );
  </script>
<?php endif; ?>	
</body>

</html>

