<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<script>
		var ssiSocialTrackingCode = 'baf27545ad0bd257';
	</script>
	<script>
		!function(s,e){s._ssiAsyncQueue=[],s._ssiAddBeacon=function(e){s._ssiAsyncQueue.push({methodName:"_ssiAddBeacon",args:[e,(new Date).getTime()]})},s._ssiLinkCustomer=function(e){s._ssiAsyncQueue.push({methodName:"_ssiLinkCustomer",args:[e,(new Date).getTime()]})},s._ssiLoginCustomer=function(e){s._ssiAsyncQueue.push({methodName:"_ssiLoginCustomer",args:[e,(new Date).getTime()]})},s._ssiLogPurchase=function(e){s._ssiAsyncQueue.push({methodName:"_ssiLogPurchase",args:[e,(new Date).getTime()]})};var n=e.createElement("script");n.async=1,n.src="https://app.socialsignin.net/assets/v2/js/social-tracking.js";var t=e.getElementsByTagName("script")[0];t.parentNode.insertBefore(n,t)}(window,document);
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142410135-1"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());

	 gtag('config', 'UA-142410135-1');
	</script>
</head>

<body <?php body_class(); ?>>

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
	
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark brand_bgblue fixed-top">

		<?php if ( 'container' == $container ) : ?>
			<div class="container">
		<?php endif; ?>

					<!-- Your site title as branding in the menu -->
					<?php if ( ! has_custom_logo() ) { ?>

						<?php if ( is_front_page() && is_home() ) : ?>

							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

						<?php else : ?>

							<a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" itemprop="url"><?php bloginfo( 'name' ); ?></a>

						<?php endif; ?>


					<?php } else {
						the_custom_logo();
					} ?><!-- end custom logo -->

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<!-- The WordPress Menu goes here -->
				<div id="navbarNavDropdown" class="collapse navbar-collapse">
				<?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'navbar-collapse',
						'menu_class'      => 'navbar-nav ml-auto flex-fill text-uppercase nav-fill justify-content-between',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				); ?>

				<?php
            $beamember_url = get_field('membership_link_top', 'option');
            ?>
            <a href="<?php echo get_permalink( $beamember_url ); ?>" class="btn btn-invert d-block text-center shadow-sm mx-auto mx-md-2 text-uppercase font0_8 beamember_cta"><?php the_field('membership_label_top', 'option'); ?></a>
				<?php
            $contact_url = get_field('contactus_url_top', 'option');
            ?> 
				<a href="<?php echo get_permalink( $contact_url ); ?>" class="text-center d-block icons_btn" style="font-size: 1.3rem;"><i class="fa fa-envelope" aria-hidden="true"></i></a>
				</div>

			<?php if ( 'container' == $container ) : ?>
			</div><!-- .container -->
			<?php endif; ?>

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
