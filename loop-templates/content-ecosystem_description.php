<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div id="post-<?php the_ID(); ?>" class="gridder-content">
   <div class="row align-items-start justify-content-center p-3 text-white">
      <div class="col-12 col-md-3 text-center mx-auto">
         <a href="<?php the_field('url_ecosystem'); ?>" class="imageavatar_border text-center mx-auto" target="_blank">
            <?php echo get_the_post_thumbnail( $post->ID, 'icons' ); ?>
         </a>
      </div>
      <div class="col-12 col-md-auto">
         <hr class="vr_divider m-0" />
      </div>
      <div class="col-12 col-md text-left">
         <h2 class="font-weight-bold font1_4 my-0"><?php the_title(); ?></h2>
         <a href="<?php the_field('url_ecosystem'); ?>" target="_blank" class="brand_txtyellow font0_9 mb-3"><?php the_field('url_ecosystem'); ?></a>
         <div class="mt-3">
            <?php the_content(); ?>
         </div>
      </div>
   </div>
</div>