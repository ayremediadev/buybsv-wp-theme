<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="col-6 col-md-4">
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
		<div class="row align-items-center justify-content-center archive_news_block mt-2 mb-4">
			<div class="col-12 archive_news_headerimg position-relative">
            <a href="<?php echo esc_url( get_permalink() ); ?>" class="position-relative">
               <div class="w-100 h-100 position-absolute hover-video text-center my-auto mx-auto"><i class="fa fa-play display-5 position-absolute align-items-center justify-content-center" aria-hidden="true"></i></div>
               <?php echo get_the_post_thumbnail( $post->ID, 'archive-image-size' ); ?>
            </a>
			</div>
			<div class="col-12 my-2 font-weight-bold archive_news_title">
				<?php
					the_title(
						sprintf( '<a href="%s" rel="bookmark" class="text-dark">', esc_url( get_permalink() ) ),
						'</a>'
					);
				?>
			</div>
			<div class="col-12">
				<span class="entry-date brand_txtyellow font0_8"><?php echo get_the_date('d F Y'); ?></span>
			</div>
			<div class="col-12">
				<hr class="position-relative my-3" />
			</div>
			
			<div class="col-12"><p class="my-0 text-secondary font0_9"><?php the_content(); ?></p></div>
		</div>
	</article>

</div>