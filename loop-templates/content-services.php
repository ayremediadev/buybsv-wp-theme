<?php
/**
 * Content empty partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="col-6 col-md-3">
	<div class="row align-items-start justify-content-center" id="post-<?php the_ID(); ?>">
		<div class="col-6 col-md-4 text-center my-3">
			<?php
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
						the_post_thumbnail( 'icons', array( 'class'  => 'responsive-class img-fluid w-100' ) ); // show featured image
				} 
			?>
		</div>
		<div class="col-12 text-center">
			<h4 class="brand_txtyellow mb-3 font1_1 font-weight-bold header_twoline_1rem"><?php echo get_the_title(); ?></h4>
			<p class="font0_9 mx-3"><?php echo get_the_content(); ?></p>
		</div>
	</div>
</div>