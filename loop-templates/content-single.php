<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<section class="section_block brand_bgwhite clearfix">
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" class="py-5 position-relative">
		<!-- Header -->
		<header class="brand_bggray position-relative pt-4 pb-5">
			<div class="container">
			<div class="row align-items-center justify-content-center text-center">
				<div class="col-12">
					<?php the_title( '<h1 class="entry-title m-0 p-0 font1_6 font-weight-bold mb-4">', '</h1>' ); ?>
				</div>
				<div class="col-12">
					<?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
				</div>
			</div>
			</div>
		</header>

		<div class="container">
			<div class="row align-items-start justify-content-center my-5">
				<div class="col-12">
					<div class="entry-meta float-left mr-3 mb-3 rounded brand_bggray">
						<span class="posted-on d-block p-3 text-center">
							<time datetime="<?php echo get_the_date('c'); ?>">
								<span class="date_day display-3 font-weight-bold brand_txtyellow d-block"><?php echo get_the_date('d'); ?></span>
								<span class="date_month brand_txtblue text-uppercase font0_9"><?php echo get_the_date('M'); ?></span>
								<span class="date_year brand_txtblue text-uppercase font0_9"><?php echo get_the_date('Y'); ?></span>
							</time>
						</span>
					</div><!-- .entry-meta -->
					<?php the_content(); ?>
				</div>

			</div>
		</div>

	</article>
</section>