<?php
/**
 * Template Name: Homepage
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<main role="main" class="clearfix">
  
<?php if ( get_field( 'enable_top_section_bar' ) ): ?>
    <div class="notification brand_bgyellow py-3">
      <div class="container">
        <div class="row align-items-center justify-content-between">
          <div class="col-12 col-md"><h2 class="display-5 text-white m-0 p-0 font1_6"><?php the_field('top_section_bar_content'); ?></h2></div>
          <div class="col-12 col-md-auto">
            <?php
            $topsection_url = get_field('top_section_bar_page');
            ?>
            <a href="<?php echo get_permalink( $topsection_url ); ?>" class="btn btn-primary btn-white shadow-sm"><?php the_field('top_section_bar_button_label'); ?></a>
          </div>
        </div>
      </div>
    </div>
<?php endif; ?>
    <!-- Header -->
    <header class="brand_bgblue py-5 position-relative">
      <?php 
        $featuredimage_bg = get_field('main_featured_header_background');
      ?>
      <div class="h-100 w-100 position-absolute homepage_header_bg" style="background-image: url(<?php echo $featuredimage_bg['url']; ?>);"></div>
      <div class="container h-100">
        <div class="row h-100 align-items-center py-5 my-5">
          <div class="col-lg-8">
            <h2 class="text-white mt-5 mb-3 font-weight-bold"><?php the_field('main_featured_header_title'); ?></h2>
            <?php the_field('main_featured_header_content'); ?>
            <?php
            $headerfeatured_url = get_field('main_featured_header_page');
            ?>
            <a href="<?php echo get_permalink( $headerfeatured_url ); ?>" class="btn btn-primary"><?php the_field('top_section_bar_button_label'); ?></a>
          </div>
        </div>
      </div>
    </header>
    <!-- Page Content -->
    <section class="section_block brand_bgyellow py-5">
      <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 mb-4"><h1 class="display-5 text-white text-uppercase m-0 p-0 font1_6 text-center"><?php the_field('sub_feature_title'); ?></h1></div>
            <div class="col-7 mb-3">
              <div class="embed-responsive embed-responsive-16by9">
              <?php the_field('sub_feature_youtube'); ?>
              </div>
            </div>
            <?php if( get_field('sub_feature_content') ): ?>
            <div class="col-7 mb-3">
              <?php the_field('sub_feature_content'); ?>
            </div>
            <?php endif; ?>
        </div>
      </div>
    </section>
    <section class="section_block brand_bgwhite py-5">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-12 col-md-6">
            <div class="embed-responsive embed-responsive-16by9">
              <?php the_field('last_call_youtube'); ?>
            </div>
          </div>
          <div class="col-12 col-md-6">
            <hr class="hr-left mx-0">
            <h1 class="text-uppercase m-0 p-0 font1_6 font-weight-bold"><?php the_field('last_call_title'); ?></h1>
            <?php the_field('last_call_content'); ?>
          </div>
        </div>
      </div>
    </section>


</main>

<?php get_footer(); ?>
