<?php
/**
 * Template Name: Ecosystem Page
 *
 * Template for displaying a page without sidebar customizeable header and content.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
   exit; // Exit if accessed directly.
}

get_header('custom');
$container = get_theme_mod( 'understrap_container_type' );
?>

<main role="main" class="clearfix brand_bge2">
   <?php while ( have_posts() ) : the_post(); ?>
   <!-- Header -->
   <!-- <header class="brand_bgwht position-relative py-5">
      <div class="<?php //echo esc_attr( $container ); ?>">
         <div class="row align-items-center justify-content-center text-center">
            <div class="col-12">
               <h1 class="text-uppercase mb-5 p-0 font1_6 font-weight-bold"><?php //echo get_the_title(); ?></h1>
            </div>
            <div class="col-12">
               <?php //the_content(); ?>
            </div>
         </div>
      </div>
   </header> -->


   <section class="section_block brand_bge2 pt-4 clearfix">
      <div class="<?php echo esc_attr( $container ); ?> clearfix">
        <div class="row" id="body_intro">
            <div class="col-lg-8 mx-auto text-center">
            <?php $top_content_text = get_field('top_content_text');
            echo $top_content_text; ?>
            </div>
        </div>
         
         <iframe src="https://buybsv.coinify.com"></iframe>

        <div class="row" id="account-levels">
            <div class="col-lg-12"><p class="text-center text-uppercase"> Account Levels </p></div>

            <?php if( have_rows('levels') ): 
               while( have_rows('levels') ): the_row(); 

               // vars
               $t = get_sub_field('title');
               $c = get_sub_field('title_background');
               $txt = get_sub_field('text');

               ?>
            <div class="col-md-6 col-lg-4 pb-4">
               <div class="card bg-white pt-4">
                  <div class="card-header al-no text-center text-uppercase "  style="background-color: <?php if($c){ echo $c; } ?>">
                      <p class="mb-0 font-weight-bold" style="font-size: 14px">Level</p> 
                      <p class="mb-0" style="font-size: 28px"><?php if($t){ echo $t; }?></p>
                  </div>
                  <div class="card-body pt-4 al-details">
                     <?php if($txt){ echo $txt;} ?>
                  </div>
               </div>
            </div>
           <?php endwhile; endif; ?>
         </div>
         <hr>
       <p class="text-center text-uppercase" id="wallets">Recommended Bitcoin SV <?php  //$term = get_field('ecosystem_category'); echo $term->slug;?>wallets</p>
        <ul class="gridder position-relative text-center"><!--
         <?php
            $term = get_field('ecosystem_category');
            $termslug = $term->slug;
            ?>
            <?php
            $the_query = new WP_Query( array(
               'post_type' => 'Ecosystem',
               'tax_query' => array(
                  array (
                        'taxonomy' => 'ecosystems_list',
                        'field' => 'slug',
                        'terms' => $termslug ,
                  )
               ),
               'orderby' => 'name',
               'order' => 'ASC'
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
            ?>
               <?php
               get_template_part( 'loop-templates/content', 'ecosystem_list' );
               ?>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
      --></ul>
         <?php
            $term = get_field('ecosystem_category');
            $termslug = $term->slug;
            ?>
            <?php
            $the_query = new WP_Query( array(
               'post_type' => 'Ecosystem',
               'tax_query' => array(
                  array (
                        'taxonomy' => 'ecosystems_list',
                        'field' => 'slug',
                        'terms' => $termslug ,
                        
                  )
               ),
               'orderby' => 'name',
               'order' => 'ASC'
            ) );

            while ( $the_query->have_posts() ) :
               $the_query->the_post();
            ?>
               <?php
               get_template_part( 'loop-templates/content', 'ecosystem_description' );
               ?>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>
      </div>

      <div id="prefooter">
         <div class="container">
            <div class="row py-4">
               <div class="col-lg-6" id="countries">
                  <?php if( have_rows('countries') ): 
                      while( have_rows('countries') ): the_row(); 
                      // vars
                     $title_c = get_sub_field('title');
                     $color_t = get_sub_field('title_color'); 
                     $list = get_sub_field('text');?>
                  <p class="font-weight-bold text-left text-uppercase" style="color: <?php if($color_t){ echo $color_t; } ?>; font-size: 12px;"><?php if($title_c){ echo $title_c;} ?></p>
                  <div class="row">
                  <?php if( $list ){ echo $list;} ?>
                  </div>
               <?php endwhile; endif; ?>
               </div>
               <div class="col-lg-1" ></div>
               <div class="col-lg-5" id="subscribe">

                  <?php  if( have_rows('newsletter_form') ): 
                      while( have_rows('newsletter_form') ): the_row(); 
                      // vars
                     $title_n = get_sub_field('title');
                     $color_t = get_sub_field('title_color'); 
                     $intro = get_sub_field('text');
                     $form = get_sub_field('form'); ?>
               <p class="font-weight-bold text-left text-uppercase" style="color: <?php if($color_t){ echo $color_t; } ?>; font-size: 12px;"><?php if($title_n){ echo $title_n;} ?></p>
                  <p style="font-size: 12px;"><?php if($intro){ echo $intro; } ?></p>
                  <div class="newsletter-form">
                  <?php if( $form ){ echo $form;} ?>
                  </div>
               <?php endwhile; endif; ?>
               </div>
            </div>
         </div>
      </div>

      <!-- <div class="<?php //echo esc_attr( $container ); ?> clearfix">
         <div class="row align-items-center justify-content-center my-3 clearfix">
            <div class="col-12">
               <?php //the_field('ecosystem_footer_content'); ?>
            </div>
         </div>
      </div> -->
   </section>

<?php if ( get_field( 'display_welcome_video' ) ): ?>
   <section class="section_block brand_bgyellow py-5">
      <div class="container">
         <div class="row align-items-center justify-content-center">
               <div class="col-12 mb-4"><h2 class="display-5 text-white text-uppercase m-0 p-0 font1_6 text-center"><?php the_field('last_call_welcome_title', 'option'); ?></h2></div>
               <div class="col-7 mb-3">
                  <div class="embed-responsive embed-responsive-16by9">
                     <?php the_field('last_call_welcome_video', 'option'); ?>
                  </div>
               </div>
         </div>
      </div>
   </section>
<?php endif; ?>
<?php if ( get_field( 'display_membership_bar' ) ): ?>
   <section class="section_block brand_bggray py-5">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-12 col-md">
               <p class="p-0 m-0"><?php the_field('join_now_content_footer', 'option', false, false); ?></p>
            </div>
            <div class="col-12 col-md-auto">
            <?php
            $joinmembership_url = get_field('join_now_cta_url_footer', 'option');
            ?>
            <a href="<?php echo get_permalink( $joinmembership_url ); ?>" class="btn btn-invert shadow-sm"><?php the_field('join_now_cta_label_footer', 'option'); ?></a>
            </div>
         </div>
      </div>
   </section>
<?php endif; ?>
    <?php endwhile; // end of the loop. ?>
</main>

<?php get_footer(); ?>
