<?php
/**
 * Template Name: Video Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>


<main role="main" class="clearfix">
   <?php while ( have_posts() ) : the_post(); ?>






<section class="section_block brand_bgwhite clearfix">
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" class="py-5 position-relative">
		<!-- Header -->
		<header class="brand_bggray position-relative pt-4 pb-5">
			<div class="container">
            <div class="row align-items-center justify-content-center text-center">
            <?php

            $featuredvideo = get_field('featured_video');

            if( $featuredvideo ): 

               // override $post
               $post = $featuredvideo;
               setup_postdata( $post ); 

               ?>
               <div class="col-12">
                  <?php the_title( '<h1 class="entry-title m-0 p-0 font1_6 font-weight-bold mb-4">', '</h1>' ); ?>
               </div>
               <div class="col-12 col-md-7 mb-3">
                  <div class="embed-responsive embed-responsive-16by9">
                     <?php the_field('youtube_link'); ?>
                  </div>
               </div>
               <div class="col-10 mb-3">
                  <?php the_content(); ?>
               </div>
               <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
            <?php endif; ?>
            </div>
			</div>
		</header>

    <?php endwhile; // end of the loop. ?>
		<div class="container">
			<div class="row align-items-start justify-content-center my-5">
				<?php
				// WP_Query arguments
				$args = array (
					'post_type'              => array( 'videos' ),
					'post_status'            => array( 'publish' ),
					'nopaging'               => true,
					'order'                  => 'DESC',
					'orderby'                => 'date',
				);

				$services = new WP_Query( $args );

				if ( $services->have_posts() ) {
					while ( $services->have_posts() ) {
						$services->the_post();
				?>
									<?php

									/*
										* Include the Post-Format-specific template for the content.
										* If you want to override this in a child theme, then include a file
										* called content-___.php (where ___ is the Post Format name) and that will be used instead.
										*/
									get_template_part( 'loop-templates/content', 'videos' );
									?>
				<?php		
					}
				} else {
				?>
				<?php
				}
				// Restore original Post Data
				wp_reset_postdata();
				?>
			</div>
		</div>

	</article>
</section>

<?php if ( get_field( 'display_welcome_video' ) ): ?>
   <section class="section_block brand_bgyellow py-5">
      <div class="container">
         <div class="row align-items-center justify-content-center">
               <div class="col-12 mb-4"><h2 class="display-5 text-white text-uppercase m-0 p-0 font1_6 text-center"><?php the_field('last_call_welcome_title', 'option'); ?></h2></div>
               <div class="col-12 col-md-7 mb-3">
                  <div class="embed-responsive embed-responsive-16by9">
                     <?php the_field('last_call_welcome_video', 'option'); ?>
                  </div>
               </div>
         </div>
      </div>
   </section>
<?php endif; ?>
<?php if ( get_field( 'display_membership_bar' ) ): ?>
   <section class="section_block brand_bggray py-5">
      <div class="container">
         <div class="row align-items-center justify-content-between">
            <div class="col-12 col-md">
               <p class="p-0 m-0"><?php the_field('join_now_content_footer', 'option', false, false); ?></p>
            </div>
            <div class="col-12 col-md-auto">
            <?php
            $joinmembership_url = get_field('join_now_cta_url_footer', 'option');
            ?>
            <a href="<?php echo get_permalink( $joinmembership_url ); ?>" class="btn btn-invert shadow-sm"><?php the_field('join_now_cta_label_footer', 'option'); ?></a>
            </div>
         </div>
      </div>
   </section>
<?php endif; ?>


</main>

<?php get_footer(); ?>
