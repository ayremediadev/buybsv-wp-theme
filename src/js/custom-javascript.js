var navbar = jQuery( '.navbar' ).height() + 16;
jQuery('body').css('margin-top',navbar );

jQuery(window).scroll(function() {
  if (jQuery(document).scrollTop() > 70) {
    jQuery('.navbar').addClass('shrink');
  } else {
    jQuery('.navbar').removeClass('shrink');
  }
});

// jQuery(document).ready(function() {
//    var acookie = getCookie("viewed_cookie_policy");
//    if (!acookie) {
//       jQuery( ".sponsor_footer" ).addClass( "cookie-law-info-bar__open" );
//    }

//    jQuery( "#cookie-law-info-bar a" ).click(function() {
//    jQuery( ".sponsor_footer" ).removeClass( "cookie-law-info-bar__open" );
//    });

//    jQuery( "#cookie_hdr_showagain" ).click(function() {
//    jQuery( ".sponsor_footer" ).addClass( "cookie-law-info-bar__open" );
//    });

// });

var maxHeight = 0;
jQuery(".archive_news_title").each(function(){
  if (jQuery(this).height() > maxHeight) { maxHeight = jQuery(this).height(); }
});

jQuery(".archive_news_title").height(maxHeight);

var archiveimgHeight = 0;
jQuery(".archive_news_headerimg").each(function(){
  if (jQuery(this).height() > archiveimgHeight) { archiveimgHeight = jQuery(this).height(); }
});

jQuery(".archive_news_headerimg").height(archiveimgHeight);

var ecosystemHeight = 0;
jQuery(".ecosystem_excerpt").each(function(){
  if (jQuery(this).height() > ecosystemHeight) { ecosystemHeight = jQuery(this).height(); }
});

jQuery(".ecosystem_excerpt").height(ecosystemHeight);



jQuery(function() {

  // Call Gridder
  jQuery('.gridder').gridderExpander({
      scroll: true,
      scrollOffset: 30,
      scrollTo: "panel",                  // panel or listitem
      animationSpeed: 400,
      animationEasing: "easeInOutExpo",
      showNav: false,
      onStart: function(){
          //Gridder Inititialized
      },
      onContent: function(){
          //Gridder Content Loaded
      },
      onClosed: function(){
          //Gridder Closed
      }
  });

});